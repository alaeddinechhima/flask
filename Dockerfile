FROM ubuntu:latest
MAINTAINER Ala Eddine CHHIMA "alaeddine.chhima@riminder.net"
RUN apt-get update -y
RUN apt-get install -y python-pip python-dev build-essential
COPY . /app
WORKDIR /app
EXPOSE 7345
RUN pip install -r requirements.txt
ENTRYPOINT ["python"]
CMD ["manage.py","runserver"]
