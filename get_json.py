import sys
import json

data ={
    "family": "real-flask",
    "containerDefinitions": [
        {
            "image": "008363129475.dkr.ecr.us-east-2.amazonaws.com/real-flask:"+str(sys.argv[1]),
            "name": "real-flask",
            "cpu": 1024,
            "memory": 300,
            "essential": True,
            "portMappings": [
                {
                    "containerPort": 7345,
                    "hostPort": 80
                }
		]
        }
	]
}

with open('real-task.json','wb') as out :
 	json.dump(data,out,
                      indent=4, sort_keys=True,
                      separators=(',', ': '), ensure_ascii=False)
