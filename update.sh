#!/bin/bash
SERVICE_NAME="real-flask"
IMAGE_VERSION=${BUILD_NUMBER}
TASK_FAMILY="real-flask"

# Create a new task definition for this build
sed -e "s;%CI_COMMIT_SHA%;${CI_COMMIT_SHA};g" flask-signup.json > flask-signup-v_${CI_COMMIT_SHA}.json
aws ecs register-task-definition --family real-flask --cli-input-json file://flask-signup-v_${CI_COMMIT_SHA}.json

# Update the service with the new task definition and desired count
TASK_REVISION=`aws ecs describe-task-definition --task-definition real-flask | egrep "revision" | tr "/" " " | awk '{print $2}' | sed 's/"$//'`
DESIRED_COUNT=`aws ecs describe-services --services ${SERVICE_NAME} | egrep "desiredCount" | tr "/" " " | awk '{print $2}' | sed 's/,$//'`
echo ${TASK_FAMILY}:${TASK_REVISION}

aws ecs update-service --cluster real-flask --service ${SERVICE_NAME} --task-definition ${TASK_FAMILY}:${TASK_REVISION} --desired-count 1
